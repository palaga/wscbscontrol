#! /usr/bin/env python2

from urwid import *

from datetime import datetime
import multiprocessing
import time, os, pickle, signal
import ConfigParser
import konijn
import bootje

__version__ = "0.2"

class Controller (WidgetWrap):
    def __init__ (self, boot, bootup_time, outrate, inrate, size):
        self.outrate   = outrate
        self.inrate    = inrate
        self.size      = size
        self.info      = multiprocessing.Queue()
        self.time      = Text("")
        self.avg_out   = Text("")
        self.avg_in    = Text("")
        self.sizebox   = Text("")
        self.action    = Text("")
        self.hb        = Text("")
        self.timer     = Text("")
        self.tvalue    = 0
        self.btime     = bootup_time
        self.boot      = boot

        pile           = Pile([self.avg_in, self.avg_out, self.sizebox, self.time, self.action, self.timer])
        fill           = Filler(pile, 'top')
        frame          = LineBox(Frame(fill, footer=self.hb), "Controller")
        self.__super.__init__(frame)


    def heartbeat (self, loop, data):
        (interval, index) = data
        states            = "-\\|/"
        index             = (index + 1) % len(states)
        self.hb.set_text(states[index])
        loop.set_alarm_in(interval, self.heartbeat, (interval, index))


    def nextUpdate (self, loop, value):
        self.tvalue = 0 if self.tvalue == 0 else self.tvalue - 1
        self.timer.set_text("Next update in %d second%s" % (self.tvalue, 's' if self.tvalue != 1 else ''))
        loop.set_alarm_in(1, self.nextUpdate, None)


    def update (self, action, avg_out, avg_in, size, interval):
        self.tvalue = interval
        time = '-' if action == '-' else datetime.now().strftime("%H:%M:%S.%f")
        self.time.   set_text("Last action:    %s" % time)
        self.action. set_text("Action taken:   %s" % action)
        self.avg_out.set_text("Avg out rate:   %s" % avg_out)
        self.avg_in. set_text("Avg in rate:    %s" % avg_in)
        self.sizebox.set_text("Avg Queue size: %s" % size)


    def __callback (self, value):
        args = self.info.get()
        self.update(*args)


    def start (self, loop, interval):
        fd = loop.watch_pipe(self.__callback)
        p  = multiprocessing.Process(target=self.__controller_process, args=(fd, interval))
        p.start()
        return p


    def __controller_process (self, fd, interval):
        self.info.put(('-', '-', '-', '-', interval))
        os.write(fd, "1")     # notify callback
        time.sleep(interval)  # delay startup
        #length = lambda x: float(len(x))
        #getavg = lambda x, length: sum(x)/length if length != 0 else 0.0
        while True:
            try:
                length        = float(len(self.outrate))
                avg_out       = sum(self.outrate)/length if length != 0 else 0.0
                length        = float(len(self.inrate))
                avg_in        = sum(self.inrate)/length if length != 0 else 0.0
                length        = float(len(self.size))
                size          = sum(self.size)/length if length != 0 else 0.0

                (action,boot) = self.boot.run_cycle(avg_out, avg_in, size)
                newInterval   = self.btime if boot else interval

                self.info.put((action, avg_out, avg_in, size, newInterval))
                os.write(fd, "1") # notify callback
            finally:
                time.sleep(newInterval)
        os.close(fd)



class PlotBox (WidgetWrap):
    def __init__ (self, label, nbars = 10, style = ('empty', 'barcolor', 'empty')):
        self.label    = label       # Top label
        self.nbars    = nbars       # number of bars
        self.data     = [0] * nbars # Data array

        self.graph    = BarGraph(style)
        self.header   = Text("%s: -" % label)
        frame         = Frame(self.graph, header=self.header)
        self.graph.set_data([[0]], 1)
        self.__super.__init__(frame)


    def update (self, value, top = 'max'):
        box    = lambda x: [x]
        boxall = lambda l: map(box, l)

        # Update text
        self.header.set_text("%s: %s" % (self.label, str(value)))

        # Update bar graph
        self.data.pop(0)         # Remove front
        self.data.append(value)  # Add value to back
        bars = boxall(self.data) # Prepare bars
        top  = top if top != 'max' else max(self.data + [1])
        self.graph.set_data(bars, top)



class PlotStack (WidgetWrap):
    def __init__ (self, outrate, inrate, size, statistics_length):
        self.bindings = {}
        self.pile     = Pile([])
        self.info     = multiprocessing.Queue()
        self.outrate  = outrate
        self.inrate   = inrate
        self.size     = size
        self.slength  = statistics_length
        self.options  = self.pile.options('weight', 1)
        frame         = Frame(self.pile)
        self.__super.__init__(frame)


    def addPlot (self, name, style, binding):
        plot = PlotBox(name, style=style)
        self.bindings[binding] = plot
        self.pile.contents.append((plot, self.options))


    def getBindings (self):
        return self.bindings


    def start_graphs (self, loop, interval, username, password, group):
        fd = loop.watch_pipe(self.__callback)
        p  = multiprocessing.Process(target=self.__data_process, args=(fd, interval, username, password, group))
        p.start()
        return p


    def __data_process (self, fd, interval, username, password, group):
        while True:
            try: # for whenever the queue does not exists...
                info = konijn.getInfo(username, password, group)
                self.info.put(info)

                self.outrate.append(info['out_rate'])
                self.inrate.append(info['in_rate'])
                self.size.append(info['queue_length'])
                if len(self.outrate) > self.slength:
                    self.outrate.pop(0)
                    self.inrate.pop(0)
                    self.size.pop(0)
                os.write(fd, "1") # notify that there is something on the queue
            except:
                pass
            finally:
                time.sleep(interval)
        os.close(fd)


    def __callback (self, info):
        info = self.info.get()
        for key, plot in self.bindings.items():
            plot.update(info[key])



class EC2InfoBox (WidgetWrap):
    def __init__ (self, boot, content = []):
        self.walker   = SimpleFocusListWalker(content)
        self.listbox  = ListBox(self.walker)
        self.boot     = boot
        self.info     = multiprocessing.Queue()
        header        = self.create_item("ID", "IP address", "State")
        frame         = Frame(self.listbox, header=header)
        box           = LineBox(frame, "EC2 nodes")
        self.__super.__init__(box)


    def update (self, instances):
        ec2nodes = map(self.generate_item, instances)
        del self.walker[0:len(self.walker)] # clear list
        for i in ec2nodes:
            self.walker.append(i)
        return instances


    def generate_item (self, i):
        item = self.create_item(i.id, i.ip_address, i.state)
        if i.state in ['stopped', 'terminated']:
            item = AttrWrap(item, 'red')
        elif i.state in ['stopping', 'terminating', 'pending']:
            item = AttrWrap(item, 'yellow')
        else:
            item = AttrWrap(item, 'green')
        return item


    def create_item (self, ident, ip, state):
        return Text("%10s %15s %10s" % (ident, ip, state))


    def __callback (self, value):
        instances = self.info.get()
        self.update(instances)


    def start (self, loop, interval):
        fd = loop.watch_pipe(self.__callback)
        p  = multiprocessing.Process(target=self.__ec2box_process, args=(fd, interval))
        p.start()
        return p


    def __ec2box_process (self, fd, interval):
        while True:
            try:
                instances = self.boot.listInstances()
                self.info.put(instances)
                os.write(fd, "1") # notify callback
            except:
                pass
            finally:
                time.sleep(interval)
        os.close(fd)


if __name__ == "__main__":
    procs = []
    cfg   = ConfigParser.SafeConfigParser()
    cfg.read("assignment.cfg")
    siget = lambda x: cfg.getint("schip", x)
    sfget = lambda x: cfg.getfloat("schip", x)
    rget  = lambda x: cfg.get("rabbitmq", x)
    b     = bootje.Bootje(cfg)
    b.connect()

    manager = multiprocessing.Manager()
    inrate  = manager.list()
    outrate = manager.list()
    size    = manager.list()

    def catch_key (key):
        if key in ['q', 'Q']:
            for proc in procs:
                proc.terminate()
            raise ExitMainLoop()


    palette = [
        # name           foreground     background      monochrome
        ('head'       ,  'light red'  , 'light blue'  , 'white'      ),
        ('normal_txt' ,  'white'      , 'black'       , ''           ),
        ('complete'   ,  ''           , 'dark green'  , ''           ),
        ('red'        ,  ''           , 'dark red'    , ''           ),
        ('blue'       ,  ''           , 'dark blue'   , ''           ),
        ('green'      ,  ''           , 'dark green'  , ''           ),
        ('yellow'     ,  ''           , 'yellow'      , ''           ),
        ('barcolor'   ,  ''           , 'dark green'  , ''           ),
        ('empty'      ,  ''           , 'black'       , ''           ),
        ('hl'         ,  'dark red'   , ''            , ''           )
    ]

    style = lambda x: ('empty', x, 'empty')


    # Create plotstack
    pstack  = PlotStack(outrate, inrate, size, 10)
    pstack.addPlot("In rate",    style('red'),    'in_rate')
    pstack.addPlot("Queue size", style('green'),  'queue_length')
    pstack.addPlot("Delivered",  style('yellow'), 'delivered')
    pstack.addPlot("Out rate",   style('blue'),   'out_rate')


    # Create ec2 node list
    ec2box = EC2InfoBox(b)


    # Create infobox
    infobox = Controller(b, siget('bootup_time'), outrate, inrate, size)


    # Compose
    btxt    = Filler(Padding(BigText("SCHIP %s" % __version__, Thin6x6Font()), 'center', width='clip'), 'top')
    columns = Columns([ec2box, infobox])
    rows    = Pile([(6, btxt), columns])
    columns = Columns([(40, pstack), rows])
    frame   = Frame(columns)

    gdata   = manager.dict()
    loop    = MainLoop(frame, palette, unhandled_input = catch_key)

    procs.append(pstack.start_graphs(loop, siget('graphs'), rget("username"), rget("password"), rget("group")))
    procs.append(infobox.start(loop, siget('info')))
    procs.append(ec2box.start(loop, siget('ec2')))
    infobox.heartbeat(loop, (sfget('heartbeat'),0))
    infobox.nextUpdate(loop, None)


    loop.run()
    b.close()

