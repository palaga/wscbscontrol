#! /usr/bin/env python2

import boto.ec2
import ConfigParser
import time
import base64
from datetime import datetime

class Bootje:
  def __init__ (self, cfg):
    self.conn = None
    aget = lambda x: cfg.get("aws", x)
    pget = lambda x: cfg.getfloat("parameters", x)
    pbget = lambda x: cfg.getboolean("parameters", x)
    self.started           = []
    self.access_key        = aget("access_key")
    self.secret_access_key = aget("secret_access_key")
    self.ami               = aget("ami")
    self.key               = aget("key")
    self.region            = aget("region")
    self.instance_type     = aget("instance_type")
    self.security_group    = aget("security_group")
    self.group             = aget("group")

    self.avg_factor        = pget("avg_factor")
    self.min_ratio         = pget("min_ratio")
    self.min_out_rate      = pget("min_out_rate")
    self.size_base         = pget("size_base")
    self.max_instances     = pget("max_instances")
    self.dry_run           = pbget("dry_run")


  def startInstance (self):
    reservation = self.conn.run_instances(
      self.ami,
      key_name        = self.key,
      instance_type   = self.instance_type,
      security_groups = [self.security_group])

    instances = reservation.instances
    addOwner  = lambda x: x.add_tag("InstanceOwner", self.group)
    map(addOwner, instances)
    return instances


  def run_cycle (self, avg_out, avg_in, size):
      ratio    = (avg_in / avg_out) if avg_out > 0.0 else 0.0
      min_size = (self.avg_factor * avg_out) + self.size_base
      output   = None
      boot     = True

      if size > min_size:
          output = "Queue size > %.2f, BOOT!" % min_size
      elif avg_out < self.min_out_rate and size > 0: # don't spawn if nothing is in the queue
          output = "Avg. out < %.2f, BOOT!"   % self.min_out_rate
      elif ratio > self.min_ratio:
          output = "Ratio > %.2f, BOOT!"      % self.min_ratio
      else:
          boot   = False
          output = "Within limits, SLEEP!"

      if not self.dry_run and boot and len(self.started) < self.max_instances:
          self.started = self.started + self.startInstance()

      return (output, boot)


  def terminateStarted (self):
      self.terminateInstance(self.started)


  def terminateInstance (self, instances):
    extract_id = lambda x: x.id
    return self.conn.terminate_instances(map(extract_id, instances))


  def listInstances (self):
    tmp = self.conn.get_all_reservations(filters={"tag:InstanceOwner": self.group})
    tmp = map(lambda x: x.instances, tmp)
    tmp = reduce(lambda x,y: x + y, tmp)
    return tmp


  def getUptime (self, instance):
    fmt = "%Y-%m-%dT%H:%M:%S.%fZ"
    return abs(datetime.strptime(instance.launch_time, fmt) - datetime.now())


  def connect (self):
    self.conn = boto.ec2.connect_to_region(
      self.region,
      aws_access_key_id=self.access_key,
      aws_secret_access_key=self.secret_access_key)


  def close (self):
    self.conn.close()


  def __enter__ (self):
    self.connect()
    return self


  def __exit__ (self, type, value, traceback):
    self.close()



if __name__ == "__main__":
  cfg  = ConfigParser.SafeConfigParser()
  cfg.read("assignment.cfg")

  with Bootje(cfg) as b:
    #print(b.startInstance())
    #print(b.listInstances())
    print("Kill running instances")
    print(b.terminateInstance([i for i in b.listInstances() if i.state == "running"]))
    #b.run_cycle()
  #  getInfo = lambda x: (b.getUptime(x).total_seconds(), x.state, x.ip_address)
  #  print(map(getInfo, b.listInstances()))
