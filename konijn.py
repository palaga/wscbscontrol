#! /usr/bin/env python2

import requests

def getJSON (username, password, group, state = "RAW"):
    tag = "%s:TweetString:%s" % (group, state)
    url = "http://flightcees.lab.uvalight.net:15672/api/queues/soa/%s" % tag
    req = requests.get(url, auth = (username, password))
    return req.json()


def getInfo (username, password, group, state = "RAW"):
    obj = getJSON(username, password, group, state)
    return {
        'queue_length': obj['messages'],
        'out_rate':     obj['message_stats']['deliver_get_details']['rate'],
        'in_rate':      obj['message_stats']['publish_details']['rate'],
        'delivered':    obj['message_stats']['deliver_get']
    }


if __name__ == "__main__":
    import pprint as pp, time, ConfigParser
    cfg  = ConfigParser.SafeConfigParser()
    cfg.read("assignment.cfg")

    rget  = lambda x: cfg.get("rabbitmq", x)
    user  = rget("username")
    passw = rget("password")
    group = rget("group")
    pp.pprint(getJSON(user, passw, group))

